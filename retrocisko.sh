#!/bin/bash

################# retrocisko.sh #####################
# 		Instalación de RA y packs de roms
##################################################### 

# Definición de variables
NOMBRE_APP="RETROARCH-CISKO"
LOG_FILE="/var/log/retrocisko.log"
CONFIGURACION="https://gitlab.com/txemoto/retrocisko/raw/master/config.txt"
CONFIGURACION_FICHERO="config.txt"
INSTALACION_FICHERO="RetroarchCisko.tar.gz"
MEGA_FICHERO="https://gitlab.com/txemoto/retrocisko/raw/master/megatools"
SCRIPT_DIR="/storage/.config/scripts"
RETROARCH_EXEC="/storage/.kodi/retroarch/retroarch"
ENTWARE_EXEC="/storage/.opt/bin/opkg"
CARPETA_DESCARGA="/storage/roms/"
TAMANOTOTAL_ROMS=47185920 #45GB

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Comprobamos si hay dispositivos externos para hacer un enlace simbólico
function CheckDispositivoExterno () {

dispext=""

# Comprobamos si /storage/roms es un enlace simbólico
if [ -L /storage/roms ];then
	if [ ! -d /storage/roms ]; then
		# Es un enláce simbólico que no existe
		while true
		do	
			Cabecera 
			echo -e "\n\a No se detecta el dispostivo externo con las roms. ¿Quieres utilizar la memoria interna para instalar las ROMs?\n"
			read -p $'¿Si/No? \033[0;31m(S|N)\033[0m?: ' opcion
			case $opcion in
				s|S|SI|Si|si)
					rm -rf /storage/roms;break;;
				n|N|No|NO|no)
					MostrarError "Inserta el dispositivo externo y vuelve a ejecutar el script";break;;
				*) echo "$opcion no es una opcion válida.";
					pause;;
			esac
		done
	fi
else
	
	# Si no existe la carpeta la creamos
	mkdir -p /storage/roms

	# No hay un enlace simbólico creado. Miramos si hay dispositivos externos conectados
	ndisp=$(ls -l /var/media/ | grep -c ^d)
	if [ $ndisp -gt 0 ];then
		while true
		do	
			Cabecera 
			echo -e "\n\a Hemos detectado al menos un dispositivo de almacenamiento externo. \n ¿Deseas copiar las roms en él?\n"
			read -p $'¿Si/No? \033[0;31m(S|N)\033[0m?: ' opcion
			case $opcion in
				s|S|SI|Si|si)
					while true
					do
						Show_menu_EXT  # accede al menú 
						Read_input_EXT # espera la respuesta del usuario
						if [ $salir -eq 1 ]; then break; fi
					done

					if [ "$dispext" != "" ]; then
						# Ha elegido un dispositivo extraible
						# Movemos la carpeta roms a la unidad externa
						mkdir -p /storage/roms/ # Por si a caso no existe la carpeta
						mv -f /storage/roms/ $dispext

						#Borramos la carpeta de roms antes de hacer el enlace simbólico
						rm -rf /storage/roms
						#Hacemos el enlace simbólico
						ln -s "$dispext/roms" /storage/roms
						return
					fi
					;;
				n|N|No|NO|no) 			
					break;;
				*) echo "$opcion no es una opcion válida.";
					pause;;
			esac
		done
	fi
	
fi

}

function Show_menu_EXT()
{

	Cabecera 
	echo -e "\n\a Elige uno de los siguientes sistemas externos para generar el enlace simbólico:\n"
	n=0
	for carpeta in /var/media/*
	do
		let n++
		printf " %s.  %-40s - Espacio Libre: %s\n" $n $(echo $(basename $carpeta)) $(df -h $carpeta  | awk 'NR==2 {print $4}')
		
		
	done
	#let n++
	echo " "
	echo " 0.  Salir"
	echo " "
}

function Read_input_EXT() {
	local ncarpeta
	local x

	read -p "Selecciona una opcion [0 - $n] " ncarpeta
	case $ncarpeta in
		0)# Ha seleccionado salir
			salir=1;break;;
		*)
		if [ $ncarpeta -gt $n ]; then # Opción no válida
			echo "$ncarpeta no es una opcion válida."
			salir=0
			pause	 
		else
			# Ha elegido una carpeta
			x=0
			for carpeta in /var/media/*
			do
				let x++
				if [ $x -eq $ncarpeta ]; then dispext="$carpeta";salir=1; break; fi

			done
		fi
	esac

}

function BorrarRA () {

local opcion
local RAexiste

RAexiste=false
#
while true
do
	if [ "$#" -eq 0 ]; then
		Cabecera 
		echo -e "\n\a Se va a proceder a borrar la instalación de RetroArch\n"
		read -p $'¿Quieres continuar \033[0;31m(S|N)\033[0m?: ' opcion
	else
		opcion="S"
		[ -f $RETROARCH_EXEC ] && { MostrarMensaje "Borramos RetroArch"; RAexiste=true;}
	fi
	
	case $opcion in
	s|S|Si|Si|si)
	
		# Borramos los ejecutables
		rm -rf "/storage/.kodi/retroarch"

		# Borramos la carpeta /storage/.config/retroarch, menos los thumbnails y playlists
		mkdir -p /storage/.tmp/
		mv /storage/.config/retroarch/thumbnails /storage/.tmp/ 2> /dev/null
		mv /storage/.config/retroarch/playlists /storage/.tmp/ 2> /dev/null
		rm -rf /storage/.config/retroarch
		mkdir -p /storage/.config/retroarch
		mv /storage/.tmp/thumbnails /storage/.config/retroarch/ 2> /dev/null
		mv /storage/.tmp/playlists /storage/.config/retroarch/ 2> /dev/null
		rm -rf /storage/.tmp/
		
		while true
		do
			if [ "$#" -eq 0 ]; then
				Cabecera 
				echo -e "\n\a Podemos borrar las roms\n"				
				read -p $'¿Quieres borrar también las roms \033[0;31m(S|N)\033[0m?: ' opcion
			else
				opcion="N"  # La desinstalación de RA no borra las posibles roms (thumbnails y playlist) que estuvieran descargadas
			fi
			
			case $opcion in
			s|S|Si|Si|si)

				MostrarMensaje "Borramos ROMs"
				# Borramos thumbnails, playlist y roms
				rm -rf "/storage/.config/retroarch"
				rm -rf "/storage/roms"			
				break;;
			n|N|No|NO|no) 
				break;;
			*) echo "$opcion no es una opcion válida.";	pause;;
			esac
		done

		[ "$RAexiste" = true ] && { MostrarMensaje "Datos borrados correctamente" "GREEN";}
		
		if [ "$#" -eq 0 ]; then pause; fi
		return;;
	n|N|No|NO|no) return;;
	*) echo "$opcion no es una opcion válida."; pause;;
	esac
done
}

# Elegimos el servidor para descargar las ROMs
function EligeServidor () {

	while true
	do	
		Cabecera 
		while true
		do
			Show_menu_servidor  # accede al menú 
			Read_input_servidor # espera la respuesta del usuario
			if [ "$servidor" != "NO" ]; then break; fi
		done
		
		case "$servidor" in
			"MEGA")
				InstalaMEGA;break;;
			"Archive")
				break;;
			"GDrive") Instala_Entware_wget;break;;
			*) break;;
		esac
	done

}

# Elegir el servidor de descarga
function Show_menu_servidor()
{
	Cabecera 
	echo -e "\n"
	echo -e "\t 1.  MEGA (Recomendado)"
	echo -e "\t 2.  Archive.org"
	[ "$SO" = "CoreELEC" ] && echo -e "\t 3.  Google Drive"
	echo -e "\t "
	echo -e "\t 0.  Salir"
	echo -e " "
}

function Read_input_servidor() {
	local opcion
	local nop
	nop=2;[ "$SO" = "CoreELEC" ] && nop=3 
	read -p "    Selecciona una opcion [0 - $nop] " opcion
	case $opcion in
		1) servidor="MEGA";;
		2) servidor="Archive";;
		3) 
			if [ "$SO" = "CoreELEC" ];then
				servidor="GDrive"
			else
				echo "$opc no es una opcion válida."
				servidor"NO"
				pause
			fi;;
		0) servidor="";;
		*) echo "$opc no es una opcion válida.";servidor="NO"
		   pause;;	
	esac
}

function InstalaMEGA (){

	# Comprobamos si tenemos el ejecutable descargado
	if [ ! -f "$SCRIPT_DIR/megatools" ]; then

		# Descargamos la última versión disponible a la carpeta temporal
		wget ${ncert} -q -O "$SCRIPT_DIR/megatools" $MEGA_FICHERO -T 10
		if [ $? -ne 0 ]; then
			MostrarError "Error en la descarga del fichero megatools"
		else	
			chmod +x "$SCRIPT_DIR/megatools"
		fi
	fi
}


# Descargamos la configuración de URL de instalación de RA y de sistemas
function DescargaCFG() {
	
	mkdir -p $CARPETA_DESCARGA
	
	rm -f $CARPETA_DESCARGA$CONFIGURACION_FICHERO
	
	# Descargamos la última versión disponible a la carpeta temporal
	wget ${ncert} -q -O $CARPETA_DESCARGA$CONFIGURACION_FICHERO $CONFIGURACION -T 10
	if [ $? -ne 0 ]; then
		MostrarError "Error en la descarga del fichero de configuración"
	else
		# Tratamos la configuración leída
		IFS=$'\n'
		nsis=0; sistemas="";primeralinea=0
		while IFS='' read -r line || [[ -n "$line" ]]; do
			if [ $primeralinea -eq 0 ]; then
				# Primera línea la URL del .tar de instalación
				INSTALACION_URL=$line
				primeralinea=1
			else
				# Resto de líneas, los sistemas. Como en busybox no hay arrays, los metemos en una varible con separador ";"
				sistemas="$sistemas"$'\n'"$line"
				let nsis++
			fi
		done < $CARPETA_DESCARGA$CONFIGURACION_FICHERO
		#echo "URL:$INSTALACION_URL"
		#echo "sistemas:$sistemas";pause
		rm -f $CARPETA_DESCARGA$CONFIGURACION_FICHERO
	fi;[ ! -d /storage/.lib ] && exit
	
}

# Mostramos un menú para descargar las roms
function DescargarROMs() {

local opcion

	Cabecera 
	
	if [ ! -f $RETROARCH_EXEC ]; then
		MostrarMensaje "RetroArch no está instalado" "RED"
		pause
	else
	
		# Comprobaciones de la carpeta /storage/roms
		CheckDispositivoExterno
		
		while true
		do
			Cabecera 
			freedisk=$(df /storage/roms  | awk 'NR==2 {print $4}')
			if [ $freedisk -gt $TAMANOTOTAL_ROMS ];then
				echo -e "\n\a ¿Quieres descargar todos los packs de roms? \n Si dices que no, puedes instalar los packs individualmente\n"
				read -p $'¿Si/No? \033[0;31m(S|N)\033[0m?: ' opcion
			else
				opcion="N"
			fi
			case $opcion in
				s|S|SI|Si|si)
					DescargaInstalaSistema "TODO"
					break;;
				n|N|No|NO|no) 			
					salir=0
					while true
					do
						Show_menu_ROM  # accede al menú 
						Read_input_ROM # espera la respuesta del usuario
						if [ $salir -eq 1 ]; then return;fi
					done
					break;;
				*) echo "$opcion no es una opcion válida.";
					pause;;
			esac
		done
	fi
}

function Show_menu_ROM()
{
	Cabecera
	lista=""
	echo -e "\t (x) - Número de juegos descargados de cada sistema"
	echo -e "\n"
	IFS=$'\n';n=0
#	echo $sistemas
	for sis in $sistemas
	do
		let n++
		sist=$(echo $sis | cut -d';' -f1)
		peso=$(echo $sis | cut -d';' -f6)
		nroms=0
		if [ -d "/storage/roms/$sist" ]; then nroms=$(ls /storage/roms/$sist | wc -l);fi
#	    echo -e "\t $n.\t$sist ($nroms) -$peso-"
#		printf "\t %+2s.  %-50s (%+4s)  [%+7s]\n" $n $sist $nroms $peso
		lista="$lista$(printf '\t %+2s.  %-50s (%+4s)  [%+7s]' "$n" "$sist" "$nroms" "$peso")\n"
		
	done
	echo -e "$lista"
	let n++
	echo " "
	echo -e "\t 0.  Salir"
	echo " "
}

function Read_input_ROM() {
	local sistema
	read -p "    Selecciona una opcion [0 - $nsis] " sistema
	case $sistema in
		0) 	salir=1;return;; # Ha elegido salir
		*)
		if [ $sistema -gt $nsis ]; then # Ha seleccionado opción no válida
			echo "$sistema no es una opcion válida."
			salir=0
			pause	 
		else
			# Ha elegido un sistema
			DescargaInstalaSistema
		fi
	esac
	clear
}

function DescargaInstalaSistema() {

	IFS=$'\n';n=0
	#echo $sistemas; pause

	# Elegimos el servidor de dónde descargar. Solo una vez
	[ "$servidor" = "" ] && EligeServidor; [ $servidor = "" ] && return
	
	Cabecera
	
	if [ "$servidor" != "" ];then
		for sis in $sistemas
		do
			let n++
			if [ "$#" -eq 1 ] || [ $n -eq $sistema ]; then # Si es nuestra opción o han seleccionado instalar todo
			
				elec_nom=$(echo "$(echo $sis | cut -d';' -f1)")
				elec_url=$(echo "$(echo $sis | cut -d';' -f2)")
				elec_id=$(echo "$(echo $sis | cut -d';' -f3)")
				elec_fg=$(echo "$(echo $sis | cut -d';' -f4)")
				elec_mega=$(echo "$(echo $sis | cut -d';' -f5)")
				MostrarMensaje "Descargamos: $elec_nom.tar.gz"
				rm -f "$CARPETA_DESCARGA$elec_nom.tar.gz"
				sec_inicio=$(date --date="$(date +"%Y-%m-%d %H:%M:%S")" +"%s")
				# Descargamos el pack que corresponda
				DescargaPack
			
				# Tenemos que descomprimir el fichero
				if [ ! -f "$CARPETA_DESCARGA$elec_nom.tar.gz" ];then
					MostrarMensaje "Error en la descarga del fichero $elec_nom.tar.gz" "RED"
				else
					MostrarMensaje "Descomprimimos: $elec_nom.tar.gz"
					tar -xzf "$CARPETA_DESCARGA$elec_nom.tar.gz" -C /
					if [ $? -ne 0 ]; then
						MostrarMensaje "Error al descomprimir $elec_nom.tar.gz" "RED"
					else
						sec_fin=$(date --date="$(date +"%Y-%m-%d %H:%M:%S")" +"%s")
						MostrarMensaje "Pack de roms $elec_nom instalada correctamente ($(date -d@$(($sec_fin-$sec_inicio)) -u +%H:%M:%S))" "GREEN"
					fi
				fi

				rm -f "$CARPETA_DESCARGA$elec_nom.tar.gz"
			fi
		done
	
		pause
	fi
}

function DescargaPack () {

	# Descargamos el pack del lugar que corresponda
	case $servidor in
		MEGA)
			$SCRIPT_DIR/megatools dl --path $CARPETA_DESCARGA $elec_mega;;
		Archive)
			wget ${ncert} -c "$elec_url" -P $CARPETA_DESCARGA  -T 10;;
		GDrive)
			gdrive_download $elec_id "$CARPETA_DESCARGA$elec_nom.tar.gz" $elec_fg
	esac
}


# Necesario instalar Entware (installentware) y luego el paquete wget (opkg install wget)
Instala_Entware_wget() {

	wget_instalado=false
	if [ "$SO" = "CoreELEC" ]; then #Solo disponible en CoreELEC
		if [ -f $ENTWARE_EXEC ]; then
			# Entware ya instalado
			wget_instalado=true
			if [ ! -f /storage/.opt/bin/wget ]; then
				MostrarMensaje "Instalamos wget-ssl"
				opkg install wget
				MostrarError "Después de instalar wget debes volver a iniciar el script (solo una vez)"
			fi
		else
			while true
			do
				Cabecera 
				echo -e "\n\a Para descargar desde Google Drive es necesario instalar la herramienta wget-ssl de Entware. Será necesario un reinicio de la máquina \n Si no quieres instalar Entware las descargas se harán desde otro servidor más lento ¿Quieres instalar Entware (wget-ssl)? "
				read -p $'¿Si/No? \033[0;31m(S|N)\033[0m?: ' opcion
	
				case $opcion in
					s|S|SI|Si|si)
						MostrarMensaje "Instalamos Entware"
						installentware
						echo "Es necesario reiniciar la máquina."
						echo "Pulsa una tecla para reiniciar"
						pause
						reboot
						exit;;
					n|N|No|NO|no) 			
						break;;
					*) echo "$opcion no es una opcion válida.";
						pause;;
				esac
			done
		fi
	fi
}

function BorraEntware () {

	while true
	do
		Cabecera 
		if [ ! -f $ENTWARE_EXEC ];then
			MostrarMensaje "Entware no se encuentra instalado"; pause
		else
			echo -e "\n\a Se va a proceder a desinstalar Entware. \n Será necesario reiniciar el equipo.\n ¿Estás de acuerdo?\n"
			read -p $'¿Si/No? \033[0;31m(S|N)\033[0m?: ' opcion
			case $opcion in
				s|S|SI|Si|si)
					rm -rf /opt/*
					echo "Es necesario reiniciar el equipo"
					echo "Pulsa una tecla para reiniciar"
					pause
					reboot
					break;;
				n|N|No|NO|no) 			
					break;;
				*) echo "$opcion no es una opcion válida.";
					pause;;
			esac
		fi
	done

}

function IniciarRA() {

local opcion
#
while true
do
	Cabecera 
	if [ "$#" -eq 0 ]; then
		echo -e "\n\a Se va a proceder a la instalación de RetroArch (Cisko)\n"
		read -p $'¿Quieres continuar \033[0;31m(S|N)\033[0m?: ' opcion
	else
		opcion="S"
		MostrarMensaje "Instalamos RA en modo silencioso" "WHITE"
	fi
	case $opcion in
		s|S|Si|Si|si)
		
		# Borramos RA antes de hacer la instalación
		BorrarRA "NOUI"
		
		# Borramos el fichero si ya está descargado
		rm -f $CARPETA_DESCARGA$INSTALACION_FICHERO

		#Descargamos RA
		MostrarMensaje "Descargamos el fichero de instalación: $INSTALACION_FICHERO" "WHITE"

		wget ${ncert} -q -c "$INSTALACION_URL$INSTALACION_FICHERO" -P "$CARPETA_DESCARGA" -T 10
		if [ $? -ne 0 ]; then
			MostrarError "Error en la descarga del fichero $INSTALACION_URL$INSTALACION_FICHERO \n\a Comprueba la conexión a Internet${NC}"		
		else
			# El fichero se ha descargado bien
			MostrarMensaje "Fichero $INSTALACION_FICHERO descargado correctamente" "WHITE"
	
				# Restauramos el fichero descargado
			MostrarMensaje "Restauramos el fichero $INSTALACION_FICHERO"
			tar -xzf "$CARPETA_DESCARGA$INSTALACION_FICHERO" -C /
			if [ $? -ne 0 ]; then
				MostrarError "Error al descomprimir $INSTALACION_URL$INSTALACION_FICHERO"
			else
				MostrarMensaje "Instalación concluida correctamente" "GREEN"
				pause;clear
				MostrarMensaje "Eres un verdadero juaker si has llegado hasta aquí. ¡¡¡¡Enhorabuena!!!! \n\nPor expreso deseo de los creadores, no está permitido hablar en el grupo de la manera de llegar hasta aquí. \n\nSi lo haces serás expulsado del grupo" "RED"
				pause
			fi
		fi
		# Borramos el fichero si ya está descargado
		rm -f $CARPETA_DESCARGA$INSTALACION_FICHERO		
		break;;
		n|N|No|NO|no) break;;
		*) echo "$opcion no es una opcion válida.";
		   pause;;
	esac
done
}


function gdrive_download () {

#echo "id: $1 - fout: $2 - large: $3"; pause

	if [ "$3" -eq 1 ];then
		# Es un fichero grande
		CONFIRM=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
#echo "CONFIRM=$CONFIRM";pause
		wget  --quiet --show-progress --no-check-certificate --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2
		rm -rf /tmp/cookies.txt
	else
#echo "fichero corto"; pause
		wget --quiet  --show-progress --no-check-certificate -r "https://docs.google.com/uc?export=download&id=$1" -O $2
	fi
}

#
# Muestra el menú principal
#
function Show_menu()
{
	Cabecera 
	echo -e "\n"
	echo -e "\t 1.  Descargar packs de roms"
	echo -e "\t 2.  Borrar Retroarch y ROMs"
	[ -f $ENTWARE_EXEC ] && echo -e "\t 3.  Borrar Entware"
	echo -e "\t "
	echo -e "\t 0.  Salir"
	echo -e " "
}
#
# Purpose - Get input via the keyboard and make a decision using case..esac 
#
function Read_input() {
	local opcion
	local nop
	nop=2;[ -f $ENTWARE_EXEC ] && nop=3 
	#read -p $'    Selecciona una opcion \033[0;31m[0 - $nop)]\033[0m ' opcion
	read -p "    Selecciona una opcion [0 - $nop] " opcion
	case $opcion in
		1) DescargarROMs;;
		2) BorrarRA;;
		3) 
			if [ -f $ENTWARE_EXEC ];then
				BorraEntware
			else
				echo "$opc no es una opcion válida.";
				pause;
			fi;;
		0) clear; exit 0;;
		*) echo "$opc no es una opcion válida.";
		   pause;;	
	esac
	clear
}

# Mandar un mensaje a Kodi
function MessageToKodi() {

	if [ "$#" -eq 1 ]; then # Si nos pasan un segundo parámetro son los milisegundos que quieren 
		kodi-send -a "Notification($NOMBRE_APP,$1, 5000)" >/dev/null
		
	else
		kodi-send -a "Notification($NOMBRE_APP,$1,$2)" >/dev/null
	fi
}

#
# Función pause
#
function pause(){
	local message="$@"
	echo "$@"
#	sleep 1
	[ -z $message ] && message="presiona [Enter] para continuar..."
	read -p "$message" readEnterKey
}

function Cabecera(){
	# http://www.network-science.de/ascii/    -big-
	local h="$@"
	local TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
	clear
	echo -e "\e[32m${GREEN} -----------------------------------------------------------------------------\e[0m"
	echo -e "\e[32m${GREEN} | |  __ \    | |              /\            | |      / ____(_)   | |         |\e[0m"
	echo -e "\e[32m${GREEN} | | |__) |___| |_ _ __ ___   /  \   _ __ ___| |__   | |     _ ___| | _____   |\e[0m"
	echo -e "\e[32m${GREEN} | |  _  // _ \ __| '__/ _ \ / /\ \ | '__/ __| '_ \  | |    | / __| |/ / _ \  |\e[0m"
	echo -e "\e[32m${GREEN} | | | \ \  __/ |_| | | (_) / ____ \| | | (__| | | | | |____| \__ \   < (_) | |\e[0m"
	echo -e "\e[32m${GREEN} | |_|  \_\___|\__|_|  \___/_/    \_\_|  \___|_| |_|  \_____|_|___/_|\_\___/  |\e[0m"
	echo -e "\e[32m${GREEN} |                                                                            |\e[0m"
	echo -e "\e[32m${GREEN} |                                                     https://t.me/mecoolki  |\e[0m"
	echo -e "\e[32m${GREEN}  ----------------------------------------------------------------------------\e[0m"
	echo "$TIMESTAMP $h" >> $LOG_FILE; [ ! -d /storage/.lib ] && exit
}

#
# Comprobamos si hay Internet
#
function CompruebaInternet() {

wget -q --spider http://google.com

if [ $? -ne 0 ]; then
	MostrarError "Es necesario disponer de conexión a Internet{NC}"
fi

}


# Mostrar error

function MostrarError() {
	local TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
	clear
	echo -e "\n\a  ${RED} $1${NC}" 1>&2
	echo "$TIMESTAMP $1" >> $LOG_FILE
	pause 
	exit 1
}

#
# Mostrar mensaje
#
function MostrarMensaje() {		
	local TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`

	case "$2" in
		GREEN) # Verde
			echo -e "${GREEN}$1${NC}"
			;;
		RED) # Rojo
			echo -e "${RED}$1${NC}";
			;;
		*) # Sin color
			echo -e "$1";
			;;
	esac

	echo "$TIMESTAMP $1" >> $LOG_FILE
}

#
# Mostrar ayuda
#
function MostrarAyuda() {
	
	echo  -e "\nUso: instalaRA [-UI] [-help]"
	echo  "Instalación de Retroarch y roms en LibreELEC/CoreELEC"
	echo  "Uso:"
	echo  "	(Sin parámetros)   Instalación de RetroArch automática"
	echo  "	-UI                Menú interactivo de instalación"
	echo  "	-help              Muestra esta informción"

}

#
# Cuenta atrás
#
function CuentaAtras() {

	echo "Pulsa Ctrl+C para abortar"
	secs=$2
	while [ $secs -gt -1 ]; do
	   echo -ne "$1 en ${RED}$secs${NC} segundos\033[0K\r"
	   sleep 1
	   : $((secs--))
	done		

	printf "\n"
}

function CheckIni () {

	echo "$TIMESTAMP CheckIni:$1" >> $LOG_FILE

	case $1 in
		-INSTRA) # Mostramos la información del programa

			CuentaAtras "Instalación RetroCisko automatica" 10

			# Lanzamos la actualización automática
			IniciarRA "NO_UI";;
		*) # Parámetro introducido no válido
			;;
	esac

}

function CheckSO() {
	SO=`lsb_release | cut -d' ' -f1`
	if [ "$SO" != "LibreELEC" ] &&  [ "$SO" != "CoreELEC" ];then
		MostrarError "Este script solo se puede ejecutar en LibreELEC o CoreELEC" 
	fi
}

#
# Principal
#
#!/bin/bash

# Borramos el fichero de log si existe
rm -f "$LOG_FILE"

mkdir -p $SCRIPT_DIR

Cabecera 

#Chequeamos SO
CheckSO

# Comprobamos si tenemos internet
CompruebaInternet

# Comprobamos si tenemos instalado el wget-ssl
[ -f /storage/.opt/bin/wget ] && ncert="--no-check-certificate"

#Descargamos la configuración
DescargaCFG; CheckIni $1

while true
do
	Show_menu  # accede al menú 
	Read_input # espera la respuesta del usuario
done

exit 0