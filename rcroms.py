#!/usr/bin/python
# -*- coding: latin-1 -*-

import platform
import os, os.path
import socket
import urllib
import sys
from subprocess import call
import subprocess
import shlex
import base64

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

CONFIGURACION="https://gitlab.com/txemoto/retrocisko/raw/master/retrocisko.enc"
NOMBRE_SH="aragdfa"
CARPETA_DESCARGA="/tmp/"

def main():  
	sys.tracebacklimit = 0
	
	os.system('clear')
	
	if len(sys.argv) > 1 and sys.argv[1] == "ENC":
		#Para codificar el fichero retrocisko.sh a retrocisko.enc
		fichero="/storage/.config/scripts/retrocisko."
		data = open(fichero+"sh", "r").read()
		encoded = base64.b64encode(data)
		open(fichero+"enc", "w").write(encoded)
		print "Fichero " + fichero + "enc encriptado"
		exit()

	#Chequeamos SO
	CheckSO()

	# Comprobamos si tenemos internet
	CompruebaInternet()

	# Descargamos y desencriptamos el script
	DescargaSH()

	# Lanzamos el script. Capturamos el Ctrl C para borrar el script
	try:
		LanzaSH(sys.argv[1:])
	except KeyboardInterrupt:
		pass
	
	try:
		os.system('clear')
		os.remove(CARPETA_DESCARGA + NOMBRE_SH)
	except:
		pass

def LanzaSH(arg):
	
	if not arg:
		argumentos=""
	else:
		argumentos=arg[0]

	# Lanzamos el script
	call(shlex.split('/bin/sh ' + CARPETA_DESCARGA + NOMBRE_SH + " " + argumentos))

def pause():

	raw_input("Pulsa una tecla para continuar")
	
def DescargaSH():

	try:
		os.remove(CARPETA_DESCARGA + NOMBRE_SH)
	except:
		pass

	# Descargamos el script
	urllib.urlretrieve (CONFIGURACION, CARPETA_DESCARGA + NOMBRE_SH)

	# Comprobamos si la descarga es correcta
	if not os.path.isfile(CARPETA_DESCARGA + NOMBRE_SH):
		MostrarError ("Posible problema de acceso a Internet")

	# Leemos el fichero encriptado y lo decodificamos
	data = open(CARPETA_DESCARGA + NOMBRE_SH, "r").read()
	decoded = base64.b64decode(data)
	
	#Lo guardamos con el mismo nombre decodificado
	open(CARPETA_DESCARGA + NOMBRE_SH, "w").write(decoded)

	# Asignamos permisos de ejecución
	os.chmod(CARPETA_DESCARGA + NOMBRE_SH, 0777)
		
def CompruebaInternet():
	"""
	Para comprobar si hay conexión a Internet
	"""
	
	if not checkinternet: 
		MostrarError("Es necesario disponer de conexión a Internet")
	
	
def checkinternet(host="8.8.8.8", port=53, timeout=3):
	"""
	Host: 8.8.8.8 (google-public-dns-a.google.com)
	OpenPort: 53/tcp
	Service: domain (DNS/TCP)
	"""
	try:
		socket.setdefaulttimeout(timeout)
		socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
		return True
	except Exception as ex:
		print ex.message
		return False
	
def MostrarError(mensaje):

	os.system('clear')
	
	print bcolors.FAIL + mensaje + bcolors.ENDC
	
	sys.exit()

def CheckSO():

	os, name, version, date, arqui, _ = platform.uname()
	
	if name != "LibreELEC" and name != "CoreELEC":
		MostrarError("Este script solo se puede ejecutar en LibreELEC o CoreELEC")

# this runs when the script is called from the command line
if __name__ == '__main__':  
    main()