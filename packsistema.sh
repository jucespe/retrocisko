#!/bin/bash

CARPETA_ROMS="/storage/roms/"
CARPETA_THUMBAILS="/storage/.config/retroarch/thumbnails/"
CARPETA_PLAYLIST="/storage/.config/retroarch/playlists/"

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
if [[ $# -eq 1 ]]; then

clear	
	if [ -d "$CARPETA_ROMS$1" ] && [ -d "$CARPETA_THUMBAILS$1" ]; then
		for fichero in `find "$CARPETA_THUMBAILS$1/Named_Boxarts/" -type f -name "*.png"`; do
#echo "fichero: '$fichero'"
			fbname=$(basename "$fichero" |sed 's/.\{4\}$//')
#echo "fbname: $fbname"
#sleep 1
			if [ ! -f "$CARPETA_ROMS$1/$fbname.zip" ] ; then

				echo "No existe la rom $fbname.zip. Borramos el fichero $fbname.png"
				rm "$CARPETA_THUMBAILS$1/Named_Boxarts/$fbname.png"
			fi
		done
		tar -czvf "$1.tar.gz" "$CARPETA_PLAYLIST$1.lpl" "$CARPETA_THUMBAILS$1/" "$CARPETA_ROMS$1"
	else
		echo "Juaker, no encuentro el sistema en ROMS o Thumbnails"
	fi
else	
	echo "Juaker, tienes que poner el nombre de algún sistema que se encuentre en /storage/roms\n\a Ejemplo: packsistema.sh 'Microsoft - MSX' (entre comillas dobles si tiene espacios)"
fi
