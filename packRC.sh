#!/bin/bash

################### packRC.sh #####################
# 		Hacer pack de RetroArch
##################################################### 

CARPETA_ROMS="/storage/roms/*/"
CARPETA_TMP=/storage/tmp

mkdir -p $CARPETA_TMP

# Movemos los ficheros hiscore, savefiles, history y favorites a /tmp
[ -f /storage/.config/retroarch/content_history.lpl ] &&  mv /storage/.config/retroarch/content_history.lpl $CARPETA_TMP 
[ -f /storage/.config/retroarch/content_favorites.lpl ] && mv /storage/.config/retroarch/content_favorites.lpl $CARPETA_TMP
[ -d /storage/.config/retroarch/savefiles ] && mv /storage/.config/retroarch/savefiles $CARPETA_TMP
[ -f /storage/.config/retroarch/system/hiscore.dat ] && mv /storage/.config/retroarch/system/hiscore.dat $CARPETA_TMP
[ -d /storage/.config/retroarch/thumbnails ] && mv /storage/.config/retroarch/thumbnails $CARPETA_TMP
[ -d /storage/.config/retroarch/playlists ] && mv /storage/.config/retroarch/playlists $CARPETA_TMP

carpetas_roms=""
# Recorremos la carpeta /storage/roms para guardar los nombres de los sistemas
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for carpeta in $CARPETA_ROMS; do # recorremos las carpetas de roms
	# Conseguimos el nombre de la carpeta
	#echo $carpeta
	carp=$carpeta
	carpetas_roms="$carp;$carpetas_roms"
done

#echo $carpetas_roms

# movemos la carpeta roms a tmp
mv /storage/roms $CARPETA_TMP

# Creamos las carpetas de /storage/roms vac�as
IFS=";"
for carpeta in $carpetas_roms; do # recorremos las carpetas de roms
	# Conseguimos el nombre de la carpeta
	#echo $carpeta
	mkdir -p $carpeta
done

# Creamos las carpetas thumbnails y playlists vac�as
mkdir -p /storage/.config/retroarch/thumbnails
mkdir -p /storage/.config/retroarch/playlists 

tar czvf /storage/RetroarchCisko.tar.gz /storage/.config/retroarch /storage/.kodi/retroarch /storage/roms

# Movemos los ficheros y carpetas a su sitio
[ -f $CARPETA_TMP/content_history.lpl ] && mv $CARPETA_TMP/content_history.lpl /storage/.config/retroarch
[ -f $CARPETA_TMP/content_favorites.lpl ] && mv $CARPETA_TMP/content_favorites.lpl /storage/.config/retroarch
[ -d $CARPETA_TMP/savefiles ] && mv $CARPETA_TMP/savefiles /storage/.config/retroarch
[ -f $CARPETA_TMP/hiscore.dat ] && mv $CARPETA_TMP/hiscore.dat /storage/.config/retroarch/system/

# borramos las carpetas roms, thumbnails y playlists antes de moverlas
rm -rf /storage/roms /storage/.config/retroarch/thumbnails /storage/.config/retroarch/playlists

mv $CARPETA_TMP/roms /storage
mv $CARPETA_TMP/thumbnails /storage/.config/retroarch
mv $CARPETA_TMP/playlists /storage/.config/retroarch

rm -rf $CARPETA_TMP

clear ; echo "Fin!!!!!"